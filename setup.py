import setuptools, os.path

config = {
	'description': 'Simple Sequencer',
	'author': 'Doug Hammond',
	'url': 'https://bitbucket.org/doughammond/simpleseq',
	'download_url': 'https://bitbucket.org/doughammond/simpleseq',
	'author_email': 'doug@pacifico-hammond.co.uk',
	'version': '0.0.1',
	'install_requires': [
		# for core/server/client/web
		'RPi.GPIO',
		'Flask-SocketIO',
		'gunicorn',
		'pyzmq',
		'gevent',
		'gevent-websocket',
		'msgpack-python'
	],
	'packages': setuptools.find_packages(exclude=('tests*',)),
	'package_data': {
		'': []
	},
	'include_package_data': True,
	'scripts': [
		'bin/simpleseq-server.py'
	],
	'name': 'simpleseq'
}

def _append_data_file(path, filename):
	src_path = os.path.relpath(path, 'simpleseq')
	config['package_data'][''].append(os.path.join(src_path, filename))

def _walk_cb(exts, path, files):
	for filename in files:
		for ext in exts:
			if filename.endswith('.%s' % ext):
				_append_data_file(path, filename)

os.path.walk('simpleseq', _walk_cb, ['js', 'html', 'css'])

setuptools.setup(**config)
