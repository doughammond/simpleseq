import unittest
import mox
import zmq.green as zmq

from simpleseq import client, protocol


class Test_Client(unittest.TestCase):

	def setUp(self):
		self.mox = mox.Mox()
		self.mox.StubOutClassWithMocks(zmq, 'Context')

	def tearDown(self):
		self.mox.UnsetStubs()

	def test_init(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn():
			pass

		c = client.SeqClient('seq-server.local', _sFn)

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test_property_running(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn():
			pass

		c = client.SeqClient('seq-server.local', _sFn)

		self.assertFalse(c.running)

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test__run(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# run()
		zmq_skt_sub.setsockopt_string(zmq.SUBSCRIBE, u'')
		zmq_skt_sub.recv_multipart().AndReturn(
			('topic', '(dp0\n.')
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		sent_messages = []
		def _sFn(topic, message):
			sent_messages.append( (topic, message) )
			_sFn.c._running = False

		c = client.SeqClient('seq-server.local', _sFn)
		_sFn.c = c
		c._run()

		# ensure the __del__ handler is verified
		del _sFn.c
		del c

		self.assertSequenceEqual(
			sent_messages,
			[
				('topic', {})
			]
		)

		self.mox.VerifyAll()

	def test__run_already_running(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# run()
		# nothing called

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn(topic, message):
			_sFn.c._running = False

		c = client.SeqClient('seq-server.local', _sFn)
		c._running = True
		c._run()

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test_run_topic_force_update_valid_state_response(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# run()
		zmq_skt_sub.setsockopt_string(zmq.SUBSCRIBE, u'')
		zmq_skt_sub.recv_multipart().AndReturn(
			(
				protocol.WebEvents.FORCE_UPDATE,
				'(dp0\n.'
			)
		)
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.STATE_GET,
			'(dp0\n.'
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "S'FOO!'\np0\n.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		sent_messages = []
		def _sFn(topic, message):
			sent_messages.append( (topic, message) )
			_sFn.c._running = False

		c = client.SeqClient('seq-server.local', _sFn)
		_sFn.c = c
		c._run()

		# ensure the __del__ handler is verified
		del _sFn.c
		del c

		self.assertSequenceEqual(
			sent_messages,
			[
				(protocol.WebEvents.FORCE_UPDATE, {}),
				(protocol.WebEvents.STATE_SET, 'FOO!')
			]
		)

		self.mox.VerifyAll()

	def test_run_topic_force_update_invalid_state_response(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# run()
		zmq_skt_sub.setsockopt_string(zmq.SUBSCRIBE, u'')
		zmq_skt_sub.recv_multipart().AndReturn(
			(protocol.WebEvents.FORCE_UPDATE, '(dp0\n.')
		)
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.STATE_GET,
			'(dp0\n.'
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(False, "(dp0\nS'error'\np1\ncexceptions\nException\np2\n(S'BAR!'\np3\ntp4\nRp5\ns.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		sent_messages = []
		def _sFn(topic, message):
			sent_messages.append( (topic, message) )
			_sFn.c._running = False

		c = client.SeqClient('seq-server.local', _sFn)
		_sFn.c = c
		c._run()

		# ensure the __del__ handler is verified
		del _sFn.c
		del c

		self.assertSequenceEqual(
			sent_messages,
			[
				(protocol.WebEvents.FORCE_UPDATE, {})
				# exception was logged, not returned
			]
		)

		self.mox.VerifyAll()

	def test_direct_proxy_request(self):

		self.mox.StubOutWithMock(client, '_dispatch_request')

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# direct_proxy_request -> _dispatch_request
		client._dispatch_request(
			mox.IsA(client.SeqClient),
			protocol.ClientServerEvents.PROPERTY_GET,
			'data'
		).AndReturn('FOO!')

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn():
			pass

		c = client.SeqClient('seq-server.local', _sFn)
		result = c.direct_proxy_request(protocol.ClientServerEvents.PROPERTY_GET, 'data')

		self.assertEqual(result, 'FOO!')

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test__remote_property_get_valid_state_response(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# _remote_property_get()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_GET,
			"(dp0\nS'class'\np1\nS'obj-class'\np2\nsS'name'\np3\nS'prop-name'\np4\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nS'FOO!'\np2\ns.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn(topic, message):
			pass

		c = client.SeqClient('seq-server.local', _sFn)
		result = c._remote_property_get('obj-class', 'prop-name')

		self.assertEqual(
			result,
			'FOO!'
		)

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test__remote_property_get_invalid_state_response(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# _remote_property_get()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_GET,
			"(dp0\nS'class'\np1\nS'obj-class'\np2\nsS'name'\np3\nS'prop-name'\np4\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(False, "(dp0\n.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn(topic, message):
			pass

		c = client.SeqClient('seq-server.local', _sFn)

		self.assertRaises(
			client.UnknownRequestError,
			c._remote_property_get,
			'obj-class', 'prop-name'
		)

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test__remote_property_set(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# _remote_property_set()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_SET,
			"(dp0\nS'class'\np1\nS'obj-class'\np2\nsS'value'\np3\nS'prop-value'\np4\nsS'name'\np5\nS'prop-name'\np6\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nS'prop-value'\np2\ns.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn(topic, message):
			pass

		c = client.SeqClient('seq-server.local', _sFn)
		result = c._remote_property_set('obj-class', 'prop-name', 'prop-value')

		self.assertEqual(
			result,
			'prop-value'
		)

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test_property_bpm(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# _remote_property_get()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_GET,
			"(dp0\nS'class'\np1\nS'tra'\np2\nsS'name'\np3\nS'bpm'\np4\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nI120\ns.")
		)

		# _remote_property_set()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_SET,
			"(dp0\nS'class'\np1\nS'tra'\np2\nsS'value'\np3\nI234\nsS'name'\np4\nS'bpm'\np5\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nI234\ns.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn(topic, message):
			pass

		c = client.SeqClient('seq-server.local', _sFn)

		# property get
		bpm = c.bpm
		self.assertEqual(
			bpm,
			120
		)

		# property set
		c.bpm = 234

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test_property_div(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# _remote_property_get()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_GET,
			"(dp0\nS'class'\np1\nS'tra'\np2\nsS'name'\np3\nS'div'\np4\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nI4\ns.")
		)

		# _remote_property_set()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_SET,
			"(dp0\nS'class'\np1\nS'tra'\np2\nsS'value'\np3\nI8\nsS'name'\np4\nS'div'\np5\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nI8\ns.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn(topic, message):
			pass

		c = client.SeqClient('seq-server.local', _sFn)

		# property get
		div = c.div
		self.assertEqual(
			div,
			4
		)

		# property set
		c.div = 8

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()

	def test_property_swing(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_sub = self.mox.CreateMockAnything('zmq_skt_sub')
		zmq_ctx.socket(zmq.SUB).AndReturn(zmq_skt_sub)
		zmq_skt_sub.connect('tcp://seq-server.local:9999')

		# req/rep setup
		zmq_skt_req = self.mox.CreateMockAnything('zmq_skt_req')
		zmq_ctx.socket(zmq.REQ).AndReturn(zmq_skt_req)
		zmq_skt_req.connect('tcp://seq-server.local:9998')

		# _remote_property_get()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_GET,
			"(dp0\nS'class'\np1\nS'tra'\np2\nsS'name'\np3\nS'swing'\np4\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nF0.5\ns.")
		)

		# _remote_property_set()
		zmq_skt_req.send_multipart([
			'1', # protocol version
			protocol.ClientServerEvents.PROPERTY_SET,
			"(dp0\nS'class'\np1\nS'tra'\np2\nsS'value'\np3\nF0.75\nsS'name'\np4\nS'swing'\np5\ns."
		])
		zmq_skt_req.recv_multipart().AndReturn(
			(True, "(dp0\nS'value'\np1\nF0.75\ns.")
		)

		# zmq shutdown in client __del__
		zmq_skt_sub.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		def _sFn(topic, message):
			pass

		c = client.SeqClient('seq-server.local', _sFn)

		# property get
		swing = c.swing
		self.assertEqual(
			swing,
			0.5
		)

		# property set
		c.swing = 0.75

		# ensure the __del__ handler is verified
		del c

		self.mox.VerifyAll()
