import collections
import unittest
import mox
import zmq.green as zmq
import gevent
import struct

from simpleseq import server, protocol


class Test_Server(unittest.TestCase):

	def setUp(self):
		self.mox = mox.Mox()
		self.mox.StubOutClassWithMocks(zmq, 'Context')
		self.mox.StubOutWithMock(gevent, 'spawn')
		self.mox.StubOutWithMock(gevent, 'joinall')

		self.seq = self.mox.CreateMockAnything('self.seq')
		self.seq.transport = self.mox.CreateMockAnything('self.seq.transport')

	def tearDown(self):
		self.mox.UnsetStubs()

	def test__get_property(self):
		o = self.mox.CreateMockAnything('o')
		o.test_name = 'FOO!'

		s = self.mox.CreateMockAnything('s')
		s._property_class_map = {
			'test_class': o
		}

		self.mox.ReplayAll()

		data = {
			'class': 'test_class',
			'name':  'test_name'
		}
		result = server._get_property(s, 'cmd', data)
		self.assertSequenceEqual(
			result,
			[
				'1',
				{
					'class': 'test_class',
					'name': 'test_name',
					'value': 'FOO!'
				}
			]
		)

		self.mox.VerifyAll()

	def test__set_property(self):
		o = self.mox.CreateMockAnything('o')
		o.test_name = 'FOO!'

		s = self.mox.CreateMockAnything('s')
		s._property_class_map = {
			'test_class': o
		}

		self.mox.ReplayAll()

		data = {
			'class': 'test_class',
			'name':  'test_name',
			'value': 'BAR@'
		}
		result = server._set_property(s, 'cmd', data)
		self.assertSequenceEqual(
			result,
			[
				'1',
				{
					'class': 'test_class',
					'name': 'test_name',
					'value': 'BAR@'
				}
			]
		)
		self.assertEqual(o.test_name, 'BAR@')

		self.mox.VerifyAll()

	def test__send_state(self):
		tra = self.mox.CreateMockAnything('tra')
		tra.bpm = 'bpm'
		tra.div = 'div'
		tra.swing = 'swing'

		seq = self.mox.CreateMockAnything('seq')
		seq.sequences = 'sequences'

		s = self.mox.CreateMockAnything('s')
		s._property_class_map = {
			'tra': tra,
			'seq': seq
		}

		self.mox.ReplayAll()

		data = {}
		result = server._send_state(s, 'cmd', data)
		self.assertSequenceEqual(
			result,
			[
				'1',
				[
					{
						'class': 'tra',
						'name': 'bpm',
						'value': 'bpm'
					},
					{
						'class': 'tra',
						'name': 'div',
						'value': 'div'
					},
					{
						'class': 'tra',
						'name': 'swing',
						'value': 'swing'
					},
					{
						'class': 'seq',
						'name': 'sequences',
						'value': 'sequences'
					},
				]
			]
		)

		self.mox.VerifyAll()

	def test_init(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test_property_running(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)

		self.assertFalse(s.running)

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test_start(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# start()
		gevent.spawn(self.seq.transport.run).AndReturn('spawn-1')
		# can't find a sensible way to check that these are bound
		# methods of the SeqServer instance created below.
		gevent.spawn(mox.IsA(collections.Callable)).AndReturn('spawn-2')
		gevent.spawn(mox.IsA(collections.Callable)).AndReturn('spawn-3')

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s.start()

		self.assertTrue(s._running)
		self.assertSequenceEqual(
			s._greenlets,
			[
				'spawn-1',
				'spawn-2',
				'spawn-3',
			]
		)

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test_stop(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# stop()
		self.seq.transport.stop()

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s._running = True
		s._greenlets = [1,2,3]
		s.stop()

		self.assertFalse(s._running)
		self.assertSequenceEqual(
			s._greenlets,
			[]
		)

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test_join(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# join()
		gevent.joinall([1, 2, 3])

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s._greenlets = [1, 2, 3]
		s.join()

		self.assertSequenceEqual(
			s._greenlets,
			[]
		)

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test_step(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# step()
		self.seq.step().AndReturn( (0, 'p', 'g') )
		zmq_skt_pub.send_multipart([
			'step',
			"(dp0\nS'i'\np1\nI0\nsS'p'\np2\ng2\nsS'g'\np3\ng3\ns."
		])

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s.step()

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test__run_reqrep_command_proto_version_mismatch(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# _run_reqrep()
		zmq_skt_rep.recv_multipart().AndReturn([
			'2',
			protocol.ClientServerEvents.PROPERTY_GET,
			'(dp0\n.'
		])
		zmq_skt_rep.send_multipart([
			'',
			"(dp0\nS'error'\np1\ncexceptions\nException\np2\n(S'Request protocol version mismatch; server=1, client=2'\np3\ntp4\nRp5\ns."
		])

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s._run_reqrep()

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test__run_reqrep_command_property_get(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# _run_reqrep()
		self.mox.StubOutWithMock(server, '_get_property')
		zmq_skt_rep.recv_multipart().AndReturn([
			'1',
			protocol.ClientServerEvents.PROPERTY_GET,
			'(dp0\n.'
		])
		server._get_property(
			mox.IsA(server.SeqServer), protocol.ClientServerEvents.PROPERTY_GET, {}
		).AndReturn([
			'1',
			{
				'class': None,
				'name': None,
				'value': None,
			}
		])
		zmq_skt_rep.send_multipart([
			'1',
			"(dp0\nS'class'\np1\nNsS'value'\np2\nNsS'name'\np3\nNs."
		])

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s._run_reqrep()

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test__mon_reqrep_event_unhandled(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# _mon_reqrep()
		zmq_mon_rep.poll().AndReturn(True)
		zmq_mon_rep.recv_multipart(0).AndReturn(
			# had to dig into zmq source to figure out this encoding;
			# @see zmq.utils.monitor
			[struct.pack('=hi', 0, 0), '']
		)
		zmq_mon_rep.close()

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s._mon_reqrep()

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test__mon_reqrep_event_monitor_stopped(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# _mon_reqrep()
		zmq_mon_rep.poll().AndReturn(True)
		zmq_mon_rep.recv_multipart(0).AndReturn(
			# had to dig into zmq source to figure out this encoding;
			# @see zmq.utils.monitor
			[struct.pack('=hi', zmq.EVENT_MONITOR_STOPPED, 0), '']
		)
		zmq_mon_rep.close()

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s._mon_reqrep()

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()

	def test__mon_reqrep_event_accepted(self):

		zmq_ctx = zmq.Context()

		# pub/sub set up
		zmq_skt_pub = self.mox.CreateMockAnything('zmq_skt_pub')
		zmq_ctx.socket(zmq.PUB).AndReturn(zmq_skt_pub)
		zmq_skt_pub.bind('tcp://*:9999')

		# req/rep setup
		zmq_skt_rep = self.mox.CreateMockAnything('zmq_skt_rep')
		zmq_ctx.socket(zmq.REP).AndReturn(zmq_skt_rep)
		zmq_skt_rep.bind('tcp://*:9998')

		# req/rep monitor
		zmq_mon_rep = self.mox.CreateMockAnything('zmq_mon_rep')
		zmq_skt_rep.get_monitor_socket().AndReturn(zmq_mon_rep)

		# _mon_reqrep()
		zmq_mon_rep.poll().AndReturn(True)
		zmq_mon_rep.recv_multipart(0).AndReturn(
			# had to dig into zmq source to figure out this encoding;
			# @see zmq.utils.monitor
			[struct.pack('=hi', zmq.EVENT_ACCEPTED, 0), '']
		)
		zmq_skt_pub.send_multipart([
			'forceupdate',
			"(dp0\nS'reason'\np1\nS'client-connect'\np2\ns."
		])
		zmq_mon_rep.close()

		# zmq shutdown in client __del__
		zmq_skt_pub.close()
		zmq_skt_rep.close()
		zmq_ctx.term()

		self.mox.ReplayAll()

		s = server.SeqServer(self.seq)
		s._mon_reqrep()

		# ensure the __del__ handler is verified;
		# can't use `del s` here because of all the method
		# references created for seq event callbacks... :/
		s.__del__()

		self.mox.VerifyAll()
