import unittest
import mox
import gevent

from simpleseq import core, constants
from simpleseq.devices import compound


class Test_Sequence(unittest.TestCase):

	def test_init(self):
		s = core.Sequence(4)

		# defaults to muted
		self.assertSequenceEqual(
			s._sequence,
			[constants.MUTE, constants.MUTE, constants.MUTE, constants.MUTE]
		)

		# defaults to start of sequence
		self.assertEqual(s._index, 0)

		# test the repr works
		self.assertRegexpMatches(str(s), '<Sequence \d+>')

	def test_init_with_pattern_wrong_length(self):
		self.assertRaises(
			AssertionError,
			core.Sequence,
				4,
				initialPattern=[1]
		)

	def test_init_with_pattern_correct_length(self):
		s = core.Sequence(4, initialPattern=[1, 2, 3, 4])

		self.assertSequenceEqual(
			s._sequence,
			[1, 2, 3, 4]
		)

	def test_property_pattern(self):
		s = core.Sequence(4, initialPattern=[1, 2, 3, 4])

		self.assertSequenceEqual(
			s.pattern,
			[1, 2, 3, 4]
		)

	def test_setPattern_incorrect_length(self):
		s = core.Sequence(4, initialPattern=[1, 2, 3, 4])

		self.assertRaises(
			AssertionError,
			s.setPattern,
			[5, 6]
		)

	def test_setPattern_correct_length(self):
		s = core.Sequence(4, initialPattern=[1, 2, 3, 4])

		s.setPattern([5, 6, 7, 8])

		self.assertSequenceEqual(
			s._sequence,
			[5, 6, 7, 8]
		)

	def test_get(self):
		s = core.Sequence(4, initialPattern=[1, 2, 3, 4])

		self.assertEqual(s.get(0), 1)
		self.assertEqual(s.get(1), 2)
		self.assertEqual(s.get(2), 3)
		self.assertEqual(s.get(3), 4)

		self.assertRaises(
			IndexError,
			s.get,
			4
		)

	def test_set(self):
		s = core.Sequence(4, initialPattern=[1, 2, 3, 4])

		self.assertRaisesRegexp(
			TypeError,
			'index must be int',
			s.set,
			'a', 0
		)

		self.assertRaisesRegexp(
			ValueError,
			'value must be int or "M" or "H"',
			s.set,
			0, 'a'
		)

		s.set(0, 0)
		s.set(0, "M")
		s.set(0, "H")

	def test_step(self):
		s = core.Sequence(4, initialPattern=[1, 2, 3, 4])

		self.assertEqual(s._index, 0)
		self.assertEqual(s.step(), 1)

		self.assertEqual(s._index, 1)
		self.assertEqual(s.step(), 2)

		self.assertEqual(s._index, 2)
		self.assertEqual(s.step(), 3)

		self.assertEqual(s._index, 3)
		self.assertEqual(s.step(), 4)

		self.assertEqual(s._index, 0)
		self.assertEqual(s.step(), 1)


class Test_Transport(unittest.TestCase):

	def setUp(self):
		self.mox = mox.Mox()
		self.mox.StubOutWithMock(gevent, 'sleep')

	def tearDown(self):
		self.mox.UnsetStubs()

	def _property_setter(self, obj, prop, value):
		def _setter():
			setattr(obj, prop, value)
		return _setter

	def test_property_triggerFn(self):

		self.mox.ReplayAll()

		t = core.Transport(
			60,				# bpm
			1,				# division
			0.5,			# swing
			None			# triggerFn
		)

		def _tFn():
			pass

		self.assertIsNone(t.triggerFn)
		t.triggerFn = _tFn
		self.assertIs(t.triggerFn, _tFn)

		self.mox.VerifyAll()

	def test_tempo_properties(self):

		self.mox.ReplayAll()

		t = core.Transport(
			60,				# bpm
			1,				# division
			0.5,			# swing
			lambda: None	# triggerFn
		)

		# verify default property values
		self.assertEqual(t.bpm, 60)
		self.assertEqual(t.div, 1)
		self.assertEqual(t.swing, 0.5)

		# verify derived timing info
		self.assertSequenceEqual(t._sleeps, [1.0, 1.0])

		# attempt to set bpm to invalid type
		self.assertRaisesRegexp(
			TypeError,
			'bpm must be int or float',
			self._property_setter(t, 'bpm', 'foo')
		)
		# attempt to set bpm to invalid value
		self.assertRaisesRegexp(
			ValueError,
			'bpm must be greater than zero',
			self._property_setter(t, 'bpm', -1)
		)
		# change the bpm to twice as much; int or float values work
		t.bpm = 120
		t.bpm = 120.0

		# verify changed property value
		self.assertEqual(t.bpm, 120)

		# verify derived timing info - half the interval
		self.assertSequenceEqual(t._sleeps, [0.5, 0.5])

		# attempt to set div to invalid types
		self.assertRaisesRegexp(
			TypeError,
			'div must be int',
			self._property_setter(t, 'div', 1.25)
		)
		self.assertRaisesRegexp(
			TypeError,
			'div must be int',
			self._property_setter(t, 'div', 'foo')
		)
		# attempt to set div to invalid value
		self.assertRaisesRegexp(
			ValueError,
			'div must be greater than zero',
			self._property_setter(t, 'div', -1)
		)
		self.assertRaisesRegexp(
			ValueError,
			'div must be greater than zero',
			self._property_setter(t, 'div', 0)
		)
		# change the division to quavers; only an int will do
		t.div = 2

		# verify changed property value
		self.assertEqual(t.div, 2)

		# verify derived timing info - half the interval again
		self.assertSequenceEqual(t._sleeps, [0.25, 0.25])

		# reset bpm and division, adjust swing
		t.bpm = 60
		t.div = 1

		# attempt to set invalid swing types
		self.assertRaisesRegexp(
			TypeError,
			'swing must be float',
			self._property_setter(t, 'swing', 1)
		)
		self.assertRaisesRegexp(
			TypeError,
			'swing must be float',
			self._property_setter(t, 'swing', 'foo')
		)
		# attempt to set invalid swing values
		self.assertRaisesRegexp(
			ValueError,
			'swing must be >= 0.0 and <= 1.0',
			self._property_setter(t, 'swing', -0.4)
		)
		self.assertRaisesRegexp(
			ValueError,
			'swing must be >= 0.0 and <= 1.0',
			self._property_setter(t, 'swing', 1.1)
		)
		# set swing value; must be float
		t.swing = 0.75

		# verify changed property value
		self.assertEqual(t.swing, 0.75)

		# verify derived timing info - unequal intervals
		self.assertSequenceEqual(t._sleeps, [1.5, 0.5])

		self.mox.VerifyAll()

	def test_step(self):

		stepFn = self.mox.CreateMockAnything('stepFn')

		# we will get called back twice
		stepFn()
		stepFn()

		self.mox.ReplayAll()

		t = core.Transport(
			60,				# bpm
			1,				# division
			0.5,			# swing
			stepFn			# triggerFn
		)

		# interval pointer is in first position
		self.assertEqual(t._timer_idx, 0)

		# advance into first step
		t.step()

		# interval pointer does not change
		self.assertEqual(t._timer_idx, 0)

		# 2nd step
		t.step()

		# interval pointer does not change
		self.assertEqual(t._timer_idx, 0)

		self.mox.VerifyAll()

	def test_run(self):

		# verify that gevent.sleep was used to control the clock
		gevent.sleep(1.5)
		gevent.sleep(0.5)

		self.mox.ReplayAll()

		class TransportRecorder(object):
			def __init__(self):
				self.t = core.Transport(
					60,				# bpm
					1,				# division
					0.75,			# swing
					self.record		# triggerFn
				)
				self.r = 0

			def record(self):
				self.r += 1
				if self.r == 2:
					self.t.stop()

		tr = TransportRecorder()

		# start the transport clock
		tr.t.run()

		# check that the recorder received a callback twice
		self.assertEqual(tr.r, 2)

		self.mox.VerifyAll()

	def test_property_running(self):

		# verify that gevent.sleep was used to control the clock
		gevent.sleep(1.5)

		self.mox.ReplayAll()

		class TransportRecorder(object):
			def __init__(self):
				self.t = core.Transport(
					60,				# bpm
					1,				# division
					0.75,			# swing
					self.record		# triggerFn
				)
				self.r = []
				self.r.append(self.t.running)

			def record(self):
				self.r.append(self.t.running)
				if len(self.r) == 2:
					self.t.stop()
					self.r.append(self.t.running)

		tr = TransportRecorder()

		# start the transport clock
		self.assertEqual(tr.t.running, False)
		tr.t.run()
		self.assertEqual(tr.t.running, False)

		# check that the recorder received a callback twice
		self.assertEqual(tr.r, [False, True, False])

		self.mox.VerifyAll()


class Test_Sequencer(unittest.TestCase):

	def setUp(self):
		self.mox = mox.Mox()
		self.mox.StubOutClassWithMocks(compound, 'Gate')
		self.mox.StubOutClassWithMocks(compound, 'GatedMultiDAC')

	def tearDown(self):
		self.mox.UnsetStubs()

	def test_property_triggerFn(self):
		compound.GatedMultiDAC(
			scaling=512,
			initialValues=[constants.MUTE],
			triggerAddress=None,
			triggerInterval=1,
			gateNumbers=[22],
			address=96
		)

		self.mox.ReplayAll()
		s = core.Sequencer(
			4,		# steps
			1,		# tracks
			60,		# bpm
			1,		# division
			0.5		# swing
		)

		def _tFn():
			pass

		self.assertIsNone(s.triggerFn)
		s.triggerFn = _tFn
		self.assertIs(s.triggerFn, _tFn)

		self.mox.VerifyAll()

	def test_property_transport(self):
		compound.GatedMultiDAC(
			scaling=512,
			initialValues=[constants.MUTE],
			triggerAddress=None,
			triggerInterval=1,
			gateNumbers=[22],
			address=96
		)

		self.mox.ReplayAll()
		s = core.Sequencer(
			4,		# steps
			1,		# tracks
			60,		# bpm
			1,		# division
			0.5		# swing
		)

		self.assertIsInstance(s.transport, core.Transport)

		self.mox.VerifyAll()

	def test_property_sequences(self):
		compound.GatedMultiDAC(
			scaling=512,
			initialValues=[constants.MUTE],
			triggerAddress=None,
			triggerInterval=1,
			gateNumbers=[22],
			address=96
		)

		self.mox.ReplayAll()
		s = core.Sequencer(
			4,		# steps
			1,		# tracks
			60,		# bpm
			1,		# division
			0.5		# swing
		)

		self.assertSequenceEqual(
			s.sequences,
			[
				["M", "M", "M", "M"]
			]
		)

		self.mox.VerifyAll()

	def test_property_sequenceValue(self):
		compound.GatedMultiDAC(
			scaling=512,
			initialValues=[constants.MUTE],
			triggerAddress=None,
			triggerInterval=1,
			gateNumbers=[22],
			address=96
		)

		self.mox.ReplayAll()
		s = core.Sequencer(
			4,		# steps
			1,		# tracks
			60,		# bpm
			1,		# division
			0.5		# swing
		)

		# this is a WRITE-only property!
		self.assertRaisesRegexp(
			AttributeError,
			'unreadable attribute',
			getattr,
			s, 'sequenceValue'
		)

		def _setter(val):
			s.sequenceValue = val

		self.assertRaisesRegexp(
			TypeError,
			'Must assign a dict with structure { "track":int, "step":int, "value":\(int or "M" or "H"\) } to sequenceValue',
			_setter,
			[]
		)

		self.assertRaisesRegexp(
			ValueError,
			'Must assign a dict with structure { "track":int, "step":int, "value":\(int or "M" or "H"\) } to sequenceValue',
			_setter,
			{}
		)

		self.assertRaisesRegexp(
			ValueError,
			'Must assign a dict with structure { "track":int, "step":int, "value":\(int or "M" or "H"\) } to sequenceValue',
			_setter,
			{
				"track": "str",
				"step": 0,
				"value": 0
			}
		)

		self.assertRaisesRegexp(
			ValueError,
			'Must assign a dict with structure { "track":int, "step":int, "value":\(int or "M" or "H"\) } to sequenceValue',
			_setter,
			{
				"track": 0,
				"step": "str",
				"value": 0
			}
		)

		self.assertRaisesRegexp(
			ValueError,
			'Must assign a dict with structure { "track":int, "step":int, "value":\(int or "M" or "H"\) } to sequenceValue',
			_setter,
			{
				"track": 0,
				"step": 0,
				"value": "str"
			}
		)

		self.assertRaisesRegexp(
			ValueError,
			'track number out of range 0:0',
			_setter,
			{
				"track": 1,
				"step": 0,
				"value": 0
			}
		)

		_setter(
			{
				"track": 0,
				"step": 0,
				"value": 0
			}
		)

		self.mox.VerifyAll()

	def test_step(self):
		gmd = compound.GatedMultiDAC(
			scaling=512,
			initialValues=[constants.MUTE],
			triggerAddress=None,
			triggerInterval=1,
			gateNumbers=[22],
			address=96
		)

		gmd.write(['M']).AndReturn(([0], ['M'], [0], [0]))

		self.mox.ReplayAll()
		s = core.Sequencer(
			4,		# steps
			1,		# tracks
			60,		# bpm
			1,		# division
			0.5		# swing
		)

		self.assertEqual(
			s.step(),
			(0, [0], [0])
		)

		self.mox.VerifyAll()

	def test_setPatternForTrack(self):
		compound.GatedMultiDAC(
			scaling=512,
			initialValues=[constants.MUTE],
			triggerAddress=None,
			triggerInterval=1,
			gateNumbers=[22],
			address=96
		)

		self.mox.ReplayAll()
		s = core.Sequencer(
			4,		# steps
			1,		# tracks
			60,		# bpm
			1,		# division
			0.5		# swing
		)

		self.assertSequenceEqual(
			s._sequences[0]._sequence,
			[constants.MUTE, constants.MUTE, constants.MUTE, constants.MUTE]
		)

		self.assertRaisesRegexp(
			AssertionError,
			'incorrect pattern length',
			s.setPatternForTrack,
			0,
			[0],
		)

		self.assertRaisesRegexp(
			AssertionError,
			'incorrect track number',
			s.setPatternForTrack,
			1,
			[0, 0, 0, 0],
		)

		s.setPatternForTrack(0, [1, 2, 3, 4])

		self.assertSequenceEqual(
			s._sequences[0]._sequence,
			[1, 2, 3, 4]
		)

		self.mox.VerifyAll()
