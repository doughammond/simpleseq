import unittest
import mock

from simpleseq.devices import gpio


class Test_Output(unittest.TestCase):

	def setUp(self):
		self.m_setup = mock.patch('simpleseq.devices.gpio._gpio.setup', autospec=True).start()
		self.m_output = mock.patch('simpleseq.devices.gpio._gpio.output', autospec=True).start()

		self.addCleanup(mock.patch.stopall)

	def test_init_high(self):
		# Actions
		gpio.Output(mock.sentinel.PIN_NUMBER, 1)

		# Assertions
		self.assertSequenceEqual(self.m_setup.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1, initial=1)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [])

	def test_init_high_write_low(self):
		# Actions
		o = gpio.Output(mock.sentinel.PIN_NUMBER, 1)
		o.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_setup.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1, initial=1)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 0)
		])

	def test_init_high_write_high(self):
		# Actions
		o = gpio.Output(mock.sentinel.PIN_NUMBER, 1)
		o.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_setup.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1, initial=1)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1)
		])

	def test_init_low(self):
		# Actions
		gpio.Output(mock.sentinel.PIN_NUMBER, 0)

		# Assertions
		self.assertSequenceEqual(self.m_setup.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1, initial=0)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [])

	def test_init_low_write_low(self):
		# Actions
		o = gpio.Output(mock.sentinel.PIN_NUMBER, 0)
		o.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_setup.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1, initial=0)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 0)
		])

	def test_init_low_write_high(self):
		# Actions
		o = gpio.Output(mock.sentinel.PIN_NUMBER, 0)
		o.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_setup.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1, initial=0)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER, 1)
		])
