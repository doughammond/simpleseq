import unittest
import mock

from simpleseq.devices import mcp4728


class Test_MCP4728(unittest.TestCase):

	def setUp(self):
		self.m_smbus = mock.patch('simpleseq.devices.mcp4728.smbus_adaptor.SMBus').start()

		self.addCleanup(mock.patch.stopall)

	def test_init(self):
		# Actions
		mcp4728.MCP4728()

		# Assertions
		self.assertSequenceEqual(self.m_smbus.mock_calls, [
			mock.call(1)
		])

	def test_setAllVoltage_in_limits(self):
		# Actions
		d = mcp4728.MCP4728()
		d.setAllVoltage(100, 1000, 2000, 3000)
		
		# Assertions
		self.assertSequenceEqual(self.m_smbus.mock_calls, [
			mock.call(1),
			mock.call().write_i2c_block_data(
				# Address
				96,
				# Register
				80,
				# Data [MSB_A, LSB_A, MSB_B, ...]
				[0, 100, 3, 232, 7, 208 , 11, 184])
		])

		# verify the address sent to i2c
		self.assertEqual(96, d._address)

		# verify the register sent to i2c
		self.assertEqual(80, d._MCP4728__REG_WRITEALLDAC)

		# lets verify the byte breakdowns as sent to i2c as well...
		self.assertEqual(100,  ( 0 << 8) + 100)
		self.assertEqual(1000, ( 3 << 8) + 232)
		self.assertEqual(2000, ( 7 << 8) + 208)
		self.assertEqual(3000, (11 << 8) + 184)

	def test_setAllVoltage_below_limits(self):

		# Actions
		d = mcp4728.MCP4728()
		d.setAllVoltage(-1, -1, -1, -1)

		# Assertions
		self.assertSequenceEqual(self.m_smbus.mock_calls, [
			mock.call(1),
			mock.call().write_i2c_block_data(96, 80, [0, 0, 0, 0, 0, 0, 0, 0])
		])

	def test_setAllVoltage_above_limits(self):
		# Actions
		d = mcp4728.MCP4728()
		d.setAllVoltage(5000, 5000, 5000, 5000)

		# Assertions
		self.assertSequenceEqual(self.m_smbus.mock_calls, [
			mock.call(1),
			mock.call().write_i2c_block_data(96, 80, [0x0F, 0xFF, 0x0F, 0xFF, 0x0F, 0xFF, 0x0F, 0xFF])
		])
