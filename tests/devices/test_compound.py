import unittest
import mock

from simpleseq.devices import compound
from simpleseq import constants


class _BaseTest(unittest.TestCase):

	def setUp(self):
		self.m_output = mock.patch('simpleseq.devices.gpio.Output').start()
		self.m_mcp4728 = mock.patch('simpleseq.devices.mcp4728.MCP4728').start()

		self.addCleanup(mock.patch.stopall)

class Test_Gate(_BaseTest):

	def test_init(self):
		# Actions
		compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_properties(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)

		# Assertions
		self.assertEqual(gate.value, 0)
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_repr(self):
		# Actions
		gate = compound.Gate(123, initialValue=0)

		# Assertions
		self.assertEqual(str(gate), '<Gate 123>')
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(123),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_low_write_low(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)
		gate.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0)
			# the initial value was low, so the Output is not written again
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_low_write_low_multiple(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)
		gate.write(0)
		gate.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0)
			# the initial value was low, so the Output is not written again
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_low_write_high(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)
		gate.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0),
			mock.call().write(1)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_low_write_high_multiple(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)
		gate.write(1)
		gate.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0),
			mock.call().write(1)
			# there is only one write high, despite multiple calls
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_low_write_high_then_low(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)
		gate.write(1)
		gate.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0),
			mock.call().write(1),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_low_write_low_then_high(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=0)
		gate.write(0)
		gate.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(0),
			mock.call().write(1)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_high_write_low(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=1)
		gate.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(1),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_high_write_low_multiple(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=1)
		gate.write(0)
		gate.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(1),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_high_write_high(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=1)
		gate.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(1)
			# the initial value was high, so the Output is not written again
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_high_write_high_multiple(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=1)
		gate.write(1)
		gate.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(1)
			# the initial value was high, so the Output is not written again
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_high_write_high_then_low(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=1)
		gate.write(1)
		gate.write(0)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(1),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_initial_high_write_low_then_high(self):
		# Actions
		gate = compound.Gate(mock.sentinel.PIN_NUMBER, initialValue=1)
		gate.write(0)
		gate.write(1)

		# Assertions
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(mock.sentinel.PIN_NUMBER),
			mock.call().write(1),
			mock.call().write(0),
			mock.call().write(1)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])


class Test_Trigger(_BaseTest):

	def setUp(self):
		self.m_spawn = mock.patch('gevent.spawn_later').start()

		super(Test_Trigger, self).setUp()

	def test_repr(self):
		# Actions
		trig = compound.Trigger(123, interval=1)
		
		# Assertions
		self.assertEqual(str(trig), '<Trigger 123>')

		self.assertSequenceEqual(self.m_spawn.mock_calls, [])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(123),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

	def test_trigger(self):
		# Actions
		trig = compound.Trigger(123, interval=1)
		trig.trigger()
		
		# Assertions
		self.assertSequenceEqual(self.m_spawn.mock_calls, [
			mock.call(0, trig.write, 1),
			mock.call(0.001, trig.write, 0)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			mock.call(123),
			mock.call().write(0)
		])
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [])

class Test_GatedMultiDAC(_BaseTest):

	def test_init_with_mute(self):
		# Actions
		compound.GatedMultiDAC(
			gateNumbers=[100, 101, 102, 103],
			initialValues=[constants.MUTE, constants.MUTE, constants.MUTE, constants.MUTE]
		)

		# Assertions
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [
			mock.call(address=0x60),
			mock.call().setAllVoltage(0, 0, 0, 0)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			# If the initial values are MUTE, then turn off the gates on initialisation
			
			# output gate initialisation
			mock.call(100),
			mock.call().write(0),
			mock.call(101),
			mock.call().write(0),
			mock.call(102),
			mock.call().write(0),
			mock.call(103),
			mock.call().write(0),
			
			# output trigger initialisation
			mock.call(26),
			mock.call().write(0),
		])

	def test_init_with_hold(self):
		# Actions
		compound.GatedMultiDAC(
			gateNumbers=[100, 101, 102, 103],
			initialValues=[constants.HOLD, constants.HOLD, constants.HOLD, constants.HOLD]
		)

		# Assertions
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [
			mock.call(address=0x60),
			mock.call().setAllVoltage(0, 0, 0, 0)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			# If the initial values are HOLD, then turn off the gates on initialisation
			
			# output gate initialisation
			mock.call(100),
			mock.call().write(0),
			mock.call(101),
			mock.call().write(0),
			mock.call(102),
			mock.call().write(0),
			mock.call(103),
			mock.call().write(0),
			
			# output trigger initialisation
			mock.call(26),
			mock.call().write(0),
		])

	def test_init_with_zero_values(self):
		# Actions
		compound.GatedMultiDAC(
			gateNumbers=[100, 101, 102, 103],
			initialValues=[0, 0, 0, 0]
		)

		# Assertions
		self.assertSequenceEqual(self.m_mcp4728.mock_calls, [
			mock.call(address=0x60),
			mock.call().setAllVoltage(0, 0, 0, 0)
		])
		self.assertSequenceEqual(self.m_output.mock_calls, [
			# If the initial values are not (MUTE, HOLD), then turn on the gates on initialisation
			
			# output gate initialisation
			mock.call(100),
			mock.call().write(1),
			mock.call(101),
			mock.call().write(1),
			mock.call(102),
			mock.call().write(1),
			mock.call(103),
			mock.call().write(1),
			
			# output trigger initialisation
			mock.call(26),
			mock.call().write(0),
		])
