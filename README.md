# simpleseq #

### What is this repository for? ###

This is a simple musical sequencer designed to run on a RaspberryPi interfaced with an MCP4728 DAC (4 channel, 12bit i2c).

There are several key components in this module:

1. core - hardware device interfaces and main Sequencer logic
2. server - exposes a Sequencer object using zeromq sockets, and executes the sequence asynchronously using a gevent Greenlet
3. client - zmq client class for the server
4. web - web interface which uses client to talk to server for remote real-time display and control of the Sequencer
5. systemd configuration files - so that the whole stack can be configured to run when the RPi boots up


### How do I get set up? ###


* Configuration & Dependencies

The minimum you need to get started is a fresh python 2.7 virtual env with fabric installed. Create yourself a new fabhosts.py file:

    hosts = []
    local = '192.168.0.2'

The contents of this file is not important if you're developing only locally. You should fill in the correct remote host and local ip address details if you want to use the fabric commands to deploy to a target RaspberryPi.

You can now execute the develop command to install the development dependencies and make the simpleseq package available in your virtual env:

    fab develop


* How to run tests

    fab test

or

    fab test:watch


* Deployment instructions

    fab deploy

There are several options you can pass to the deploy command (which actually runs several sub-commands), refer to the fabfile for details.


### Contribution guidelines ###


Contributions and patches are welcome via bitbucket pull request.

Issues can be reported via the bitbucket tracker.



### Who do I talk to? ###

* Repo owner & admin = doughammond