#!/usr/bin/env python2
import logging

from simpleseq.core import Sequencer
from simpleseq.server import SeqServer

if __name__ == '__main__':
	logging.basicConfig(level=logging.INFO)
	log = logging.getLogger('simpleseq')

	sq = Sequencer(
		16,
		4,
		120,
		4,
		0.5,
		output_configs={
			'trigger_pin': 17,
			'gate_1_pin': 22,
			'gate_2_pin': 23,
			'gate_3_pin': 24,
			'gate_4_pin': 25,
		}
	)

	sv = SeqServer(sq)
	sv.start()
	sv.join()
