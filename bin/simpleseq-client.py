#!/usr/bin/env python2
import logging

from simpleseq.client import SeqClient

if __name__ == '__main__':
	noop = lambda *a, **k: None
	cl = SeqClient('localhost', noop)
