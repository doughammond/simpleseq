import functools

from flask import Flask, Blueprint, render_template, current_app
from flask_socketio import SocketIO

from simpleseq.client import SeqClient
from simpleseq import protocol


class SimpleSeqApp(Flask):
	def __init__(self, name):
		super(SimpleSeqApp, self).__init__(name)
		self.config['SECRET_KEY'] = 'oq937f6vo98w576p92c83475pbg3986'
		self.socketio = SocketIO(self)
		self.simpleseq_client = SeqClient('localhost', self.socketio.emit)

		self.socketio.on('startsequence')(self.send_sequencer_state)

		# Set up a direct web proxy for some ClientServerEvents
		for prx_message in (protocol.ClientServerEvents.PROPERTY_SET,):
			self.socketio.on(prx_message)(functools.partial(self.direct_proxy_request, prx_message))

	def direct_proxy_request(self, cmd, data):
		self.logger.info('direct_proxy_request; cmd=%s data=%r', cmd, data)
		try:
			rep = self.simpleseq_client.direct_proxy_request(cmd, data)
		except Exception, ex:
			rep = data
			rep.update({
				'error': {
					'__type__': type(ex).__name__,
					'__message__': ex.message
				},
			})
		self.socketio.emit(cmd, rep)

	def send_sequencer_state(self):
		self.simpleseq_client.start()
		self.socketio.emit(
			protocol.WebEvents.STATE_SET,
			self.simpleseq_client.direct_proxy_request(protocol.ClientServerEvents.STATE_GET, {})
		)


seq_blueprint = Blueprint(
	'simpleseq',
	__name__
)

@seq_blueprint.route('/')
def index():
	return render_template(
		'index.html',
		static_url=current_app.static_url_path
	)
