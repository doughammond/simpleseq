define(function() {
	var exports = {};


	exports.autoBind = function(objInstance) {
		for(var k in objInstance) {
			if(typeof(objInstance[k]) === 'function') {
				objInstance[k] = objInstance[k].bind(objInstance);
			}
		}
	};


	return exports;
});
