define(['jquery', 'utils'], function($, utils) {
	var exports = {};

	/**
	 * Sequencer Transport controller class
	 */
	exports.Transport = function(socket) {
		if (!(this instanceof exports.Transport)) {
			throw new Error('Transport() is a class constructor!');
		}
		utils.autoBind(this);

		this.socket = socket;
		this.propertyValidators = {
			'bpm': parseFloat,
			'div': parseInt,
			'swing': parseFloat,
		};
		this.pendingPropertyChanges = {};
		this.bindUIElements();
	};
	exports.Transport.prototype.onStep = function(data) {
		$('.step-led').removeClass('on');
		$('.step-led[data-step-id=' + data.i + ']').addClass('on');
	};
	exports.Transport.prototype.propertySet = function(data) {
		console.log('Transport received propertyset', data);
		var pKey = data.class + ':' + data.name + ':' + data.value;

		if (data.error) {
			if (this.pendingPropertyChanges[pKey]) {
				delete this.pendingPropertyChanges[pKey];
				$('#' + data.name).popover({
					title: data.error.__type__,
					content: data.error.__message__,
					placement: 'bottom',
					trigger: 'focus',
				}).popover(
					'show'
				).on('hidden.bs.popover', function() {
					$('#' + data.name).popover('destroy');
				}).data('bs.popover').tip().addClass('popover-danger');
			}
		} else {
			$('#' + data.name).val(data.value);
		}
	};
	exports.Transport.prototype.bindUIElements = function() {
		var self = this;
		$('#bpm,#div,#swing').change(function(evt) {
			var name = this.id;
			var value = self.propertyValidators[name](evt.currentTarget.value);
			if (!isNaN(value)) {
				console.log('sending ', name,'=', value);
				var pKey = 'tra:' + name + ':' + value;
				self.pendingPropertyChanges[pKey] = 1;
				self.socket.emit('propertyset', {
					'class': 'tra',
					'name': name,
					'value': value
				});
			}
		});
	}


	return exports;
});
