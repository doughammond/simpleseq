requirejs.config({
	baseUrl: requirejs.toUrl('.'),
	paths: {
		'jquery': 'lib/jquery/2.1.4/jquery.min',
		'socketio': 'lib/socketio/1.4.5/socket.io-1.4.5',
		'bootstrap': 'lib/bootstrap/3.3.5/bootstrap.min'
	},
	shim: {
		bootstrap: {
			deps: ['jquery']
		}
	}
});

requirejs(['main']);
