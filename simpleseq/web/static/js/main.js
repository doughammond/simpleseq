define(['jquery', 'bootstrap', 'socketio', 'transport'], function($, _, io, transport) {
	var socket = io.connect('http://' + document.domain + ':' + location.port);

	var tra = new transport.Transport(socket);

	socket.on('connect', function() {
		// ensure that the sequencer client in the web server is running
		socket.emit('startsequence');
	});

	socket.on('step', tra.onStep);

	socket.on('stateset', function(data) {
		console.log('stateset', data);
		data.forEach(function(item) {
			if (item.class == 'tra') {
				tra.propertySet(item);
			}
			if (item.class == 'seq' && item.name == 'sequences') {
				item.value.forEach(function(track, i) {
					track.forEach(function(value, j) {
						$('[data-track-id='+i+'][data-step-id='+j+']>input').val(value);
					});
				});
			}
		});
	});

	socket.on('propertyset', function(data) {
		if (data.error) {
			console.log(data.error);
		}

		if (data.class == 'tra') {
			tra.propertySet(data);
			return;
		}

		// console.log('propertyset', data);
		if (data.class == 'seq' && data.name == 'sequenceValue') {
			console.log(data.value);
			var track = data.value.track,
				step  = data.value.step;
			$('.pattern-value[data-track-id="'+track+'"][data-step-id="'+step+'"] > input').val(data.value.value);
		}
	});

	var propertyValidators = {
		'pattern-value': parseInt
	};

	$('.pattern-value').change(function(evt) {
		var value  = $('input', this).val(),
			valueN = propertyValidators['pattern-value'](value),
			track  = parseInt(this.dataset.trackId),
			step   = parseInt(this.dataset.stepId);
		if ((value == 'M' || value == 'H' || !isNaN(valueN)) && !isNaN(track) && !isNaN(step)) {
			socket.emit('propertyset', {
				'class': 'seq',
				'name': 'sequenceValue',
				'value': {
					track: track,
					step:  step,
					value: valueN === 0  ? 0 : valueN || value
				}
			})
		}
	});
});
