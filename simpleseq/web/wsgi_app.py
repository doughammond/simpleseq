import logging
from simpleseq.web.app import SimpleSeqApp, seq_blueprint

logging.basicConfig(level=logging.INFO)
app = SimpleSeqApp(__name__)
app.register_blueprint(seq_blueprint)
