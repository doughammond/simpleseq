
class ClientServerEvents(object):
	PROPERTY_SET = 'propertyset'
	PROPERTY_GET = 'propertyget'
	STATE_GET = 'stateget'

class WebEvents(object):
	STATE_SET = 'stateset'
	FORCE_UPDATE = 'forceupdate'
