import logging
_log = logging.getLogger('simpleseq')

# numbering modes
UNKNOWN, BOARD, BCM = 0, 1, 2

# pin states
LOW, HIGH = 0, 1

# I/O modes
IN, OUT = 0, 1

def cleanup():
	pass

def setmode(pinNumberingMode):
	pass

def setup(pinNumber, io, initial=HIGH):
	pass

def output(pinNumber, state):
	_log.debug('GPIO #%02d : %s' % (pinNumber, state))
