import logging
_log = logging.getLogger('simpleseq')

class SMBus(object):
	""" Mock SMBus interface
	"""

	def __init__(self, busNumber):
		self._busNum = busNumber

	def write_i2c_block_data(self, address, register, data):
		pdata = ', '.join(['0x%02X' % v for v in data])
		_log.debug('SMBus.write_i2c_block_data 0x%02X : 0x%02X : 0x%02X : %s' % (self._busNum, address, register, pdata))
