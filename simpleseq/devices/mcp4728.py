from simpleseq.interfaces import smbus_adaptor


class MCP4728(object):
	""" MCP4728 12-Bit 4-Channel DAC
	"""

	# Registers
	__REG_WRITEALLDAC    = 0x50

	# Constructor
	def __init__(self, bus=1, address=0x60):
		self.i2c = smbus_adaptor.SMBus(bus)
		self._address = address

	def setAllVoltage(self, volt0, volt1, volt2, volt3):
		"Sets the output voltage to the specified value"
		if volt0 > 4095:
			volt0 = 4095
		if volt0 < 0:
			volt0 = 0
		if volt1 > 4095:
			volt1 = 4095
		if volt1 < 0:
			volt1 = 0
		if volt2 > 4095:
			volt2 = 4095
		if volt2 < 0:
			volt2 = 0
		if volt3 > 4095:
			volt3 = 4095
		if volt3 < 0:
			volt3 = 0

		# Break integers into 2 bytes for sending to MCP4728
		bytes = [(volt0 >> 8) & 0xFF, (volt0) & 0xFF, (volt1 >> 8) & 0xFF, (volt1) & 0xFF,
				 (volt2 >> 8) & 0xFF, (volt2) & 0xFF, (volt3 >> 8) & 0xFF, (volt3) & 0xFF]
		self.i2c.write_i2c_block_data(self._address, self.__REG_WRITEALLDAC, bytes)
