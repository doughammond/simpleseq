from simpleseq.interfaces import gpio_adaptor as _gpio

# ensure that RPi.GPIO releases resources when we're done
import atexit as _ae
_ae.register(_gpio.cleanup)

_gpio.setmode(_gpio.BCM)

class Output(object):
	def __init__(self, pinNo, initial=_gpio.HIGH):
		_gpio.setup(pinNo, _gpio.OUT, initial=initial)
		self._pinNo = pinNo

	def write(self, value):
		_gpio.output(self._pinNo, value)
