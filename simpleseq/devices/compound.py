import gevent

from simpleseq.devices import gpio
from simpleseq.devices import mcp4728
from simpleseq import constants


class Gate(object):

	def __init__(self, address, initialValue=None):
		self._addr = address
		self._value = initialValue
		self._pin = gpio.Output(address)
		self._pin.write(initialValue)

	@property
	def value(self):
		return self._value

	def write(self, value):
		prev = self._value
		if value != self._value:
			self._pin.write(value)
			self._value = value
		return prev, value, self._value

	def __repr__(self):
		return '<Gate %02d>' % self._addr


class Trigger(Gate):
	def __init__(self, address, interval=1):
		super(Trigger, self).__init__(address, initialValue=0)
		self._interval = interval / 1000.0 # ms to s

	def __repr__(self):
		return '<Trigger %02d>' % self._addr

	def trigger(self):
		gevent.spawn_later(0, self.write, 1)
		gevent.spawn_later(self._interval, self.write, 0)


class GatedMultiDAC(object):
	""" Wrapper class for controlling an MCP4728 with Gate GPIOs and a Trigger GPIO
	"""

	def __init__(self, address=0x60, triggerAddress=None, triggerInterval=1, gateNumbers=None, initialValues=None, scaling=1):
		self._dac = mcp4728.MCP4728(address=address)
		initialValues = initialValues if isinstance(initialValues, list) else [0, 0, 0, 0]
		self.values = [
			v if v != constants.MUTE and v != constants.HOLD else 0
				for v in initialValues
		]
		self._scale = scaling
		self._gates = [
			Gate(n, int(initialValues[i] not in (constants.MUTE, constants.HOLD)))
				for i, n in enumerate(gateNumbers or [22, 23, 24, 25])
		]
		self._trigger = Trigger(triggerAddress or 26, interval=triggerInterval)
		self.write(initialValues)

	@property
	def values(self):
		return self._values

	@values.setter
	def values(self, values):
		self._values = values

	def write(self, values):
		prev_values = self.values[:]
		next_values = prev_values[:]
		gate_values = [g.value for g in self._gates]

		for i, v in enumerate(values):
			# update the DAC values, if we're not instructed to constants.HOLD or constants.MUTE
			next_values[i] = v if v != constants.MUTE and v != constants.HOLD else prev_values[i]

			# close off gates if needed before changing DAC values
			if v == constants.MUTE:
				self._gates[i].write(0)
				gate_values[i] = 0

		# ping trigger
		self._trigger.trigger()
		# write scaled values to the DAC
		self._dac.setAllVoltage(*[v*self._scale for v in next_values])

		# open gates if needed after DAC write
		for i, v in enumerate(values):
			if v != constants.MUTE and v != constants.HOLD:
				self._gates[i].write(1)
				gate_values[i] = 1

		self.values = next_values
		return (prev_values, values, next_values, gate_values)
