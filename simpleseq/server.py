import zmq.green as zmq
from zmq.utils import monitor as zmon

import gevent, gevent.monkey
gevent.monkey.patch_socket()

import logging
_log = logging.getLogger('simpleseq')

from simpleseq import serialisation
from simpleseq import protocol


def _get_property(server, cmd, data):
	c, n = data.get('class'), data.get('name')
	obj = server._property_class_map[c]
	v = getattr(obj, n)
	return '1', {
		'class': c,
		'name': n,
		'value': v
	}

def _set_property(server, cmd, data):
	_log.info('_set_property %s %r' % (cmd, data))
	c, n, v = data.get('class'), data.get('name'), data.get('value')
	obj = server._property_class_map[c]
	setattr(obj, n, v)
	return '1', {
		'class': c,
		'name': n,
		'value': v
	}

def _send_state(server, cmd, data):
	return '1', [
		_get_property(server, protocol.ClientServerEvents.PROPERTY_GET, { 'class': 'tra', 'name': 'bpm' })[1],
		_get_property(server, protocol.ClientServerEvents.PROPERTY_GET, { 'class': 'tra', 'name': 'div' })[1],
		_get_property(server, protocol.ClientServerEvents.PROPERTY_GET, { 'class': 'tra', 'name': 'swing' })[1],
		_get_property(server, protocol.ClientServerEvents.PROPERTY_GET, { 'class': 'seq', 'name': 'sequences' })[1],
	]

class SeqServer(object):
	PROTOCOL_VERSION = 1

	def __init__(self, seq):
		super(SeqServer, self).__init__()

		self._running = False
		self.seq = seq

		# avoid using bound method references as the protocol callbacks!
		self._proto = {
			protocol.ClientServerEvents.PROPERTY_GET: _get_property,
			protocol.ClientServerEvents.PROPERTY_SET: _set_property,
			protocol.ClientServerEvents.STATE_GET: _send_state
		}

		self.ctx = zmq.Context()
		self.skt_pub = self.ctx.socket(zmq.PUB)
		self.skt_pub.bind("tcp://*:9999")

		self.skt_rep = self.ctx.socket(zmq.REP)
		self.skt_rep.bind("tcp://*:9998")

		self.mon_rep = self.skt_rep.get_monitor_socket()

		self._greenlets = []

		self.seq.triggerFn = self.step

		self._property_class_map = {
			'seq': self.seq,
			'tra': self.seq.transport
		}

	def __del__(self):
		self.skt_pub.close()
		self.skt_rep.close()
		self.ctx.term()

	@property
	def running(self):
		return self._running

	def start(self):
		if not self._greenlets:
			self._running = True
			self._greenlets = [
				gevent.spawn(self.seq.transport.run),
				gevent.spawn(self._run_reqrep),
				gevent.spawn(self._mon_reqrep)
			]

	def stop(self):
		self.seq.transport.stop()
		self._running = False
		self._greenlets[:] = []

	def join(self):
		gevent.joinall(self._greenlets)
		self._greenlets[:] = []

	def step(self):
		i, p, g = self.seq.step()
		_log.info('step %02d %r %r', i, p, g)
		self.skt_pub.send_multipart([
			'step',
			serialisation.dumps({
				'i': i,
				'p': p,
				'g': g
			})
		])

	def _run_reqrep(self):
		self_version = str(self.PROTOCOL_VERSION)

		while True:
			status = ''
			rep = {}

			try:
				vn, cmd, data = self.skt_rep.recv_multipart()
				if vn != self_version:
					raise Exception('Request protocol version mismatch; server=%s, client=%s' % (self_version, vn))
				data = serialisation.loads(data)
				status, rep = self._proto[cmd](self, cmd, data)
			except Exception, ex:
				rep['error'] = ex
				_log.exception('error in req/rep handler')

			self.skt_rep.send_multipart([status, serialisation.dumps(rep)])

			if not self._running:
				break

	def _mon_reqrep(self):
		while self.mon_rep.poll():
			evt = zmon.recv_monitor_message(self.mon_rep)

			if evt['event'] == zmq.EVENT_ACCEPTED:
				self.skt_pub.send_multipart([
					protocol.WebEvents.FORCE_UPDATE,
					serialisation.dumps({
						'reason': 'client-connect'
					})
				])

			if evt['event'] == zmq.EVENT_MONITOR_STOPPED:
				break

			if not self._running:
				break

		self.mon_rep.close()
