import gevent

import logging
_log = logging.getLogger('simpleseq')

from simpleseq import constants
from simpleseq.devices import compound as dev


class Sequence(object):
	""" Class which defines a sequence of values, in order and maintains
		an active index into the sequence.
	"""

	def __init__(self, steps, initialPattern=None):
		self._steps = steps
		if initialPattern is not None:
			assert steps == len(initialPattern)
		self._sequence = initialPattern or [
			constants.MUTE
				for _ in xrange(self._steps)
		]
		self._index = 0

	@property
	def index(self):
		return self._index

	@property
	def steps(self):
		return self._steps

	@property
	def pattern(self):
		return self._sequence[:]

	def __repr__(self):
		return '<Sequence %s>' % id(self)

	def setPattern(self, pattern):
		assert len(pattern) == self._steps, 'incorrect pattern length'
		self._sequence = pattern[:]

	def get(self, index):
		return self._sequence[index]

	def set(self, index, value):
		if not isinstance(index, int):
			raise TypeError('index must be int; recevied %r' % index)

		if not (isinstance(value, int) or value in (constants.MUTE, constants.HOLD)):
			raise ValueError('value must be int or "M" or "H"; recevied %r' % value)

		self._sequence[index] = value

	def step(self):
		out = self._sequence[self._index]
		_log.debug('%s tick %02d : %r', self, self._index, out)
		self._index = (self._index + 1) % self._steps
		return out

class Transport(object):
	""" Class which moves one or more Sequences from one state to another by
		advancing Sequence values according to a transport rule, e.g.
		- regular clock
		- irregular clock
		- manual advance / external trigger
	"""

	def __init__(self, bpm, div, swing, triggerFn):
		self._bpm = self._div = self._swing = None

		self._triggerFn = triggerFn

		self._timer_idx = 0
		self._running = False
		self._sleeps = [1, 1]

		self.bpm = bpm
		self.div = div
		self.swing = swing

	@property
	def running(self):
		return self._running

	@property
	def bpm(self):
		return self._bpm
	@bpm.setter
	def bpm(self, bpm):
		if not isinstance(bpm, (int, float)):
			raise TypeError('bpm must be int or float; received %r' % bpm)
		if bpm < 0:
			raise ValueError('bpm must be greater than zero; received %r' % bpm)
		self._bpm = bpm
		self._setTempo(bpm=self._bpm)

	@property
	def div(self):
		return self._div
	@div.setter
	def div(self, div):
		if not isinstance(div, int):
			raise TypeError('div must be int; received %r' % div)
		if div <= 0:
			raise ValueError('div must be greater than zero; received %r' % div)
		self._div = div
		self._setTempo(div=self._div)

	@property
	def swing(self):
		return self._swing
	@swing.setter
	def swing(self, swing):
		if not isinstance(swing, float):
			raise TypeError('swing must be float; received %r' % swing)
		if not (0 <= swing <= 1):
			raise ValueError('swing must be >= 0.0 and <= 1.0; received %r' % swing)
		self._swing = swing
		self._setTempo(swing=self._swing)

	def _setTempo(self, bpm=None, div=None, swing=None):
		bpm = bpm or self._bpm
		div = div or self._div
		swing = swing or self._swing
		_log.info('setting tempo; bpm=%r, div=%r, swing=%r', bpm, div, swing)
		if bpm is not None and div is not None and swing is not None:
			beatSleep = 120.0 / bpm / div
			self._sleeps = [
				swing * beatSleep,
				(1-swing) * beatSleep
			]
			_log.info('calculated sleeps=%r', self._sleeps)
		else:
			self._sleeps = [1, 1]

	@property
	def triggerFn(self):
		return self._triggerFn
	@triggerFn.setter
	def triggerFn(self, fn):
		self._triggerFn = fn

	@property
	def sleep(self):
		interval = self._sleeps[self._timer_idx]
		self._timer_idx = (self._timer_idx + 1) % 2
		return interval

	def step(self):
		self._triggerFn()

	def run(self):
		self._running = True
		while self._running:
			self._triggerFn()
			gevent.sleep(self.sleep)

	def stop(self):
		self._running = False


class Sequencer(object):
	""" Main Sequencer controller. Manages:
		- Active Sequence(s)
		- Transport state
		- Propagating current values to an output Device
	"""

	def __init__(self, steps, outputs, bpm, div, swing, output_configs=None, triggerFn=None):
		output_configs = output_configs or {}

		self._sequences = [
			Sequence(steps)
				for _ in xrange(outputs)
		]

		self._transport = Transport(bpm, div, swing, triggerFn)

		self._dac = dev.GatedMultiDAC(
			address=0x60,
			triggerAddress=output_configs.get('trigger_pin'),
			triggerInterval=1,
			gateNumbers=[
				output_configs.get('gate_%d_pin' % (i+1), 22+i)
					for i in xrange(outputs)
			],
			initialValues=[
				s.get(0) for s in self._sequences
			],
			scaling=512
		)

	@property
	def triggerFn(self):
		return self.transport._triggerFn
	@triggerFn.setter
	def triggerFn(self, fn):
		self._transport.triggerFn = fn

	@property
	def transport(self):
		return self._transport

	@property
	def sequences(self):
		return [s.pattern for s in self._sequences]

	def sequenceValue(self, value):
		_errMsg = 'Must assign a dict with structure { "track":int, "step":int, "value":(int or "M" or "H") } to sequenceValue; received %r' % value
		if not isinstance(value, dict):
			raise TypeError(_errMsg)
		track = value.get('track')
		step = value.get('step')
		value = value.get('value')
		if not (isinstance(track, int) and isinstance(step, int) and (value in (constants.MUTE, constants.HOLD) or isinstance(value, int))):
			raise ValueError(_errMsg)

		if not (0 <= track < len(self._sequences)):
			raise ValueError('track number out of range 0:%d; received %r' % (len(self._sequences)-1, track))

		self._sequences[track].set(step, value)

	sequenceValue = property(fset=sequenceValue)

	def setPatternForTrack(self, trackNumber, pattern):
		assert 0 <= trackNumber < len(self._sequences), 'incorrect track number'
		self._sequences[trackNumber].setPattern(pattern)

	def step(self):
		dacData = self._dac.write([
			s.step() for s in self._sequences
		])
		return (
			(self._sequences[0].index - 1) % self._sequences[0].steps,
			dacData[2], # DAC  values
			dacData[3]  # Gate values
		)
