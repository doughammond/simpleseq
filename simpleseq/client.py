import zmq.green as zmq
import gevent, gevent.monkey
gevent.monkey.patch_socket()

import logging
_log = logging.getLogger('simpleseq')

from simpleseq import serialisation
from simpleseq import protocol


class UnknownRequestError(Exception):
	pass

def _dispatch_request(client, cmd, data):
	client.skt_req.send_multipart([
		# cast version number to string to be compatible with zmq multipart message frame
		str(client.PROTOCOL_VERSION),
		cmd,
		serialisation.dumps(data)
	])
	status, rep = client.skt_req.recv_multipart()
	rep = serialisation.loads(rep)
	_log.info('_dispatch_request cmd=%s data=%r status=%s rep=%r', cmd, data, status, rep)
	if status:
		return rep
	else:
		raise rep.get('error', UnknownRequestError())


class SeqClient(gevent.Greenlet):
	PROTOCOL_VERSION = 1

	def __init__(self, seqHost, senderFn):
		super(SeqClient, self).__init__()

		self._running = False

		self._proto = {
			protocol.ClientServerEvents.PROPERTY_GET: _dispatch_request,
			protocol.ClientServerEvents.PROPERTY_SET: _dispatch_request,
			protocol.ClientServerEvents.STATE_GET: _dispatch_request
		}

		self.ctx = zmq.Context()

		self.skt_sub = self.ctx.socket(zmq.SUB)
		self.skt_sub.connect('tcp://%s:9999' % seqHost)

		self.skt_req = self.ctx.socket(zmq.REQ)
		self.skt_req.connect('tcp://%s:9998' % seqHost)

		self.senderFn = senderFn

	def __del__(self):
		self.skt_sub.close()
		self.ctx.term()

	@property
	def running(self):
		return self._running

	def _run(self):
		if self._running:
			return

		self._running = True
		self.skt_sub.setsockopt_string(zmq.SUBSCRIBE, u'')
		while self._running:
			topic, message = self.skt_sub.recv_multipart()
			# _log.info('received pub %s : %r', topic, message)
			self.senderFn(topic, serialisation.loads(message))

			if topic == protocol.WebEvents.FORCE_UPDATE:
				try:
					state_response = self._proto[protocol.ClientServerEvents.STATE_GET](
						self, protocol.ClientServerEvents.STATE_GET, {}
					)
					self.senderFn(protocol.WebEvents.STATE_SET, state_response)
				except Exception, err:
					_log.exception('error fetching state for force update: %r', err)

	def direct_proxy_request(self, cmd, data):
		return self._proto[cmd](self, cmd, data)

	def _remote_property_get(self, obj_class, name):
		return self._proto[protocol.ClientServerEvents.PROPERTY_GET](
			self,
			protocol.ClientServerEvents.PROPERTY_GET,
			{
				'class': obj_class,
				'name': name,
			}
		).get('value')

	def _remote_property_set(self, obj_class, name, value):
		return self._proto[protocol.ClientServerEvents.PROPERTY_SET](
			self,
			protocol.ClientServerEvents.PROPERTY_SET,
			{
				'class': obj_class,
				'name': name,
				'value': value
			}
		).get('value')

	@property
	def bpm(self):
		return self._remote_property_get('tra', 'bpm')
	@bpm.setter
	def bpm(self, bpm):
		self._remote_property_set('tra', 'bpm', bpm)

	@property
	def div(self):
		return self._remote_property_get('tra', 'div')
	@div.setter
	def div(self, div):
		self._remote_property_set('tra', 'div', div)

	@property
	def swing(self):
		return self._remote_property_get('tra', 'swing')
	@swing.setter
	def swing(self, swing):
		self._remote_property_set('tra', 'swing', swing)
