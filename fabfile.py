try:
	import fabhosts
except ImportError:
	raise ImportError("""
You must create a fabhosts.py file containing a list called remote and a string called local, e.g.:
hosts = ['root@10.42.0.37']
local = '10.42.0.1'
""")


import fabric.api as f
from fabric import operations as op
import os

f.env.hosts = fabhosts.hosts

_user = os.environ.get('USER')
_source_machine_ip = fabhosts.local
_source_dir = os.getcwd()


# debugging
def ipy():
	import IPython; IPython.embed()

# LOCAL DEVELOPMENT HELPERS

def commit():
	with f.lcd(_source_dir):
		f.local("git gui")

def push():
	with f.lcd(_source_dir):
		f.local("git push")

def prepare_deploy():
	commit()
	push()

def test(watch=False):
	with f.lcd(_source_dir):
		more_args = []
		if watch:
			more_args.append("--with-watch")
		f.local(
			" ".join([
				"nosetests",
				"--no-byte-compile",
				"--traverse-namespace",
				"--with-coverage",
				"--cover-erase",
				"--cover-inclusive",
				# "--cover-branches",
				"--cover-package=simpleseq",
				# "--cover-tests",
				# "--cover-package=tests",
				"--tests=tests"
			] + more_args)
		)

def develop():
	with f.lcd(_source_dir):
		f.local('pip install --upgrade mox coverage mock funcsigs nose nose-watch')
		f.local('pip install --upgrade -e .')

# LOCAL EXECUTION HELPERS

def web():
	with f.lcd(_source_dir):
		f.local(" ".join([
				"gunicorn",
				"--worker-class geventwebsocket.gunicorn.workers.GeventWebSocketWorker",
				"--reload", # gunicorn >= 19.0
					"simpleseq.web.wsgi_app:app"
			])
		)

def server():
	with f.lcd(_source_dir):
		f.local('python bin/simpleseq-server.py')


# REMOTE INSTALLATION

def host_info():
	f.run('uname -a')

def ssh():
	op.open_shell()

def install_dependencies(targetOs='arch'):
	if targetOs == 'arch':
		f.run(
			" ".join([
				"pacman",
				"--noconfirm",
				"--noprogressbar",
				"--needed",
				"-S",
					# for cloning the simpleseq package
					"git",
					# for executing the simpleseq code
					"python2",
					# for installing the simpleseq code
					"python2-pip",
					# for building the RPi.GPIO, gevent, zmq etc libraries
					"gcc",
					# for access to the RPi hardware
					"i2c-tools",
			])
		)
	if targetOs == 'ubuntu':
		f.run(
			" ".join([
				"sudo apt-get install -y",
					# for cloning the simpleseq package
					"git",
					# for executing the simpleseq code
					"python",
					# for installing the simpleseq code
					"python-pip",
					# for building the RPi.GPIO, gevent, zmq etc libraries
					"gcc",
					# for access to the RPi hardware
					"i2c-tools"
			])
		)

def update_codebase(cloneFrom='bitbucket'):
	vcs_path = None
	with f.settings(warn_only=True):

		if cloneFrom == 'bitbucket':
			vcs_path = "git+https://doughammond@bitbucket.org/doughammond/simpleseq.git"

		if cloneFrom == 'here':
			if f.run('test -f ~/.ssh/id_*.pub').failed:
				f.run('ssh-keygen')
			f.run('ssh-copy-id %s@%s' % (_user, _source_machine_ip))
			vcs_path = 'git+ssh://%s@%s:%s' % (_user, _source_machine_ip, _source_dir)

	if not vcs_path:
		raise Exception("Don't know where to install from :(")

	f.run("pip2 install --upgrade %s" % vcs_path)

def install_service(serviceManager='systemd'):
	if serviceManager == 'systemd':
		f.put(
			local_path="systemd",
			remote_path="~/",
			mirror_local_mode=True
		)
		f.run("systemctl daemon-reload")
		f.run("mkdir -p -m 0777 /run/gunicorn")
		f.run("chmod 0777 /run/gunicorn")
		f.run("systemctl enable ~/systemd/seq.service")
		f.run("systemctl enable ~/systemd/seq-web.socket")
		f.run("systemctl enable ~/systemd/seq-web.service")

def start_service(serviceManager='systemd'):
	if serviceManager == 'systemd':
		f.run("systemctl restart --no-block seq seq-web")
		f.run("systemctl is-active seq seq-web")

def stop_service(serviceManager='systemd'):
	if serviceManager == 'systemd':
		f.run("systemctl stop seq")

def deploy(targetOs='arch', cloneFrom='bitbucket', serviceManager='systemd'):
	install_dependencies(targetOs=targetOs)
	update_codebase(cloneFrom=cloneFrom)
	install_service(serviceManager=serviceManager)
	start_service(serviceManager=serviceManager)
